package com.custardbelly.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;


public class MainFragmentActivity extends FragmentActivity implements TabBarFragment.OnFragmentInteractionListener {

    Fragment tab1Frag;
    Fragment tab2Frag;
    Fragment ctrlFrag;

    int selectedIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);

        tab1Frag = new Tab1Fragment();
        tab2Frag = new Tab2Fragment();
        ctrlFrag = new TabBarFragment();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        Fragment selectedFrag = tab1Frag;
        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey("selectedIndex")) {
                selectedFrag = savedInstanceState.getInt("selectedIndex") == 1 ? tab2Frag : tab1Frag;
            }
        }

        transaction.replace(R.id.activity_layout, selectedFrag, "tab_frag");
        transaction.replace(R.id.tab_bar_layout, ctrlFrag, "ctrlFrag");
        transaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_custom_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("selectedIndex", selectedIndex);
    }

    public void OnButtonOne() {

        this.selectedIndex = 0;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_layout, tab1Frag, "tab1frag")
                .commit();

    }

    public void OnButtonTwo() {

        this.selectedIndex = 1;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_layout, tab2Frag, "tab2frag")
                .commit();

    }
}
